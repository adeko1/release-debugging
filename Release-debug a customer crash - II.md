#Event on user side
* Release-mode compiled user application crashes on a remote machine. 
* The crush generates CRASH.DMP file.

#What we will be able to have
* The _line number_ at which crash occurs on the source code
* The _stack_ which led to the crash
* The _local variables_ with their _values_ at the time crash took place

#What we will NOT have
* Automatic delivery of crash dumps to our site.

#Action on our side
* We already have the exact executable (EXE) file, the PDB file containing the symbol information specifically generated during the compilation of the EXE, and the exact source tree which generated the EXE as well as the PDB. These have been saved and version controlled every time we make an official release. This is a must.

* We make a request to gather the CRASH.DMP file from the remote user machine to our local for debugging. This can be manual or automated; not important for the scope of this tutorial. What is important is that it is a single file containing the stack and local variables in an easily retrievable form, from within WinDbg.

* WinDbg is an old tool, now maintained and distributed freely by Microsoft (https://docs.microsoft.com/tr-tr/windows-hardware/drivers/debugger/debugging-using-windbg-preview). Have it locally available for release debugging purposes.

* Start WinDbg and set two simple options as seen here: https://bitbucket.org/adeko1/release-debugging/src/master/windbgSetting.png

* After setting the source tree and PDB file directories, use "Start debugging" > "Open dump file" to point where the CRASH.DMP file is. WinDbg requires that the PDB file be in the same directory as the exe and the CRASH.DMP file. 

* ALT+1 to switch to the command subwindow (on the top-right in the image below), and enter ".ecxr" command. You will see local variables in "Locals" subwindow on bottom-left, the stack in "Stack" on the bottom-right, and the line at which crash has happened on the top-left as seen here: https://bitbucket.org/adeko1/release-debugging/src/master/windbgFinal2.png

Done.

Return back home (https://bitbucket.org/adeko1/release-debugging/wiki/Home)