// Sandbox for understanding the exception handling behaviour of VS2017 Community Edition running on Windoz OS 10, ozgur@adeko.com, 08/06/2019

#include <pch.h>
#include <Windows.h>
#include <iostream>

/*
First the results grouped according to all available compiler options (w/o throw in catch)
(Project Properties > C/C++ > All Options > Enable C++ exceptions </EHsc, /EHs, /EHa, NO>):
-------------------------------------------------------------------------------------------

i)	 Boring: __try & __except

/EHsc					/EHs					/EHa					NO

Filtering c0000005		Filtering c0000005		Filtering c0000005		Filtering c0000005
caught in except		caught in except		destructed				caught in except 
Filtering e06d7363		Filtering e06d7363		caught in except		Filtering e06d7363
destructed				destructed				Filtering e06d7363		caught in except
caught in except		caught in except		destructed
												caught in except

ii)	 Sandwitch #1: try / __try & __except / catch (Catch does not catch anything!)

/EHsc					/EHs					/EHa					NO

Filtering c0000005		Filtering c0000005		Filtering c0000005		Filtering c0000005
caught in __except		caught in __except		destructed				caught in __except
Filtering e06d7363		Filtering e06d7363		caught in __except		Filtering e06d7363
destructed				destructed				Filtering e06d7363		caught in __except
caught in __except		caught in __except		destructed
												caught in __except

iii) Sandwitch #2: __try / try & catch / __except

/EHsc					/EHs					/EHa					NO		

Filtering c0000005		Filtering c0000005		destructed				caught in catch
caught in except		caught in except		caught in catch			caught in catch
destructed				destructed				destructed
caught in catch			caught in catch			caught in catch	

The same as iii) but with the throw statement in the catch:

/EHsc					/EHs					/EHa					NO

Filtering c0000005		Filtering c0000005		destructed				caught in catch
caught in except		caught in except		caught in catch			Filtering c0000005
destructed				destructed				Filtering c0000005		caught in except
caught in catch			caught in catch			caught in except		caught in catch
Filtering e06d7363		Filtering e06d7363		destructed				Filtering e06d7363
caught in except		caught in except		caught in catch			caught in except
												Filtering e06d7363
												caught in except

Some Low-Level Background:  
--------------------------
It's the filter-expression code that decides whether the code in the subsequent {} block will execute below.

	try { guarded body of code }
	except (filter-expression) { exception-handler block }

Please go to http://bytepointer.com/resources/pietrek_crash_course_depths_of_win32_seh.htm for more information. 

Conclusion:
-----------
	The best is to use "__try" & "__except" with the compiler option "/EHa" to catch all exceptions with gracefully destructing involved objects.
	In this specific example, optimization does not change the results, however, it is safer to turn it off, according to the literature. In case
	try/catch has to be used, say, due to legacy architecture, then from within the catch of the inner try/catch, just throw. This will cause both
	exception handlers to react one after the other.

*/

#pragma optimize("", off)

class Example { 
public:
	~Example() { std::cout << "destructed" << std::endl; }
};

int filterException(int code, PEXCEPTION_POINTERS ex) {
	std::cout << "Filtering " << std::hex << code << std::endl;
	return EXCEPTION_EXECUTE_HANDLER;
}

// Crasher code 1
void testProcessorFault() {
	Example e;
	int* p = 0;
 	*p = 42;
}

// Crasher code 2
void testCppException() {
	Example e;
	throw 42;
}

// Sandwitch crasher code 1 in-between non-standard __try/__except block to be further sandwitched in-between standard try/catch in the main
void sandwitch1_a() { 
	__try { testProcessorFault(); }
	__except (filterException(GetExceptionCode(), GetExceptionInformation())) { std::cout << "caught in __except" << std::endl; }
}

// Sandwitch crasher code 2 in-between non-standard __try/__except block to be further sandwitched in-between standard try/catch in the main
void sandwitch1_b() {
	__try { testCppException(); }
	__except (filterException(GetExceptionCode(), GetExceptionInformation())) { std::cout << "caught in __except" << std::endl; }
}

// Sandwitch crasher code 1 in-between standard try/catch block to be further sandwitched in-between non-standard __try/__except in the main
void sandwitch2_a() {
	try { testProcessorFault(); }
	catch (...) { std::cout << "caught in catch" << std::endl; throw; }
}

// Sandwitch crasher code 2 in-between standard try/catch block to be further sandwitched in-between non-standard __try/__except in the main
void sandwitch2_b() {
	try { testCppException(); }
	catch (...) { std::cout << "caught in catch" << std::endl; throw; }
}

// Un-comment only one at a time in the main function below:
//		i)		Boring
//		ii)		Sandwitch #1
//		iii)	Sandwitch #2

int main() 
{
	// Boring:	__try & __except

	//__try {	testProcessorFault(); }
	//__except (filterException(GetExceptionCode(), GetExceptionInformation())) {	std::cout << "caught in except" << std::endl;	}
	//__try {	testCppException();	}
	//__except (filterException(GetExceptionCode(), GetExceptionInformation())) {	std::cout << "caught in except" << std::endl;	}

	// Sandwitch #1:	try / __try & __except / catch

	//try { sandwitch1_a(); }
	//catch(...) { std::cout << "caught in catch" << std::endl; }
	//try { sandwitch1_b(); }
	//catch (...) { std::cout << "caught in catch" << std::endl; }

	// sandwitch #2:	__try / try & catch / __except

	__try { sandwitch2_a(); }
	__except(filterException(GetExceptionCode(), GetExceptionInformation())) { std::cout << "caught in except" << std::endl; }
	__try { sandwitch2_b(); }
	__except (filterException(GetExceptionCode(), GetExceptionInformation())) { std::cout << "caught in except" << std::endl; }

	return 0;
}
