#Event on user side
* Release-mode compiled user application crashes on a remote machine. 
* The crush generates ERRORLOG.TXT file.

#What we will be able to do
* We will find the _line number_ at which crash occurs on the source code. 

#What we will NOT have
* Call stack
* Local variables
* Function parameters
* Automatic delivery of crash dumps to our site.

#Action on our side
* We already have the exact executable (EXE) file, the PDB file containing the symbol information specifically generated during the compilation of the EXE, and the exact source tree which generated the EXE as well as the PDB. These have been saved and version controlled every time we make an official release. This is a must.

* We make a request to gather the ERRORLOG.TXT file from the remote user machine to our local for debugging. This can be manual or automated; not important for the scope of this tutorial. What is important is that it is a single file containing execution instruction pointer (EIP) and the stack in an un-readable form, unless we do not add some specific modifications to make it a bit more readable; this is also outside the scope of this document. The first two lines of ERRORLOG.TXT is as seen below:

>Test1 caused an Access Violation (0xc0000005)

>in module Test1.exe at 0023:_00402870_.

* Save the pointer (00402870) above as EIP, later to be utilized from within another tool, WinDbg as explained in the next steps.

* WinDbg is an old tool, now maintained and distributed freely by Microsoft (https://docs.microsoft.com/tr-tr/windows-hardware/drivers/debugger/debugging-using-windbg-preview). Have it locally available for release debugging purposes.

* Start WinDbg and set two simple options as seen here: https://bitbucket.org/adeko1/release-debugging/src/master/windbgSetting.png

* After setting the source tree and PDB file directories, use "Start debugging" > "Launch executable" to point the EXE file

* Activate "View > Registers" dockable windowlet and find where *eip* register is. Replace its hexadecimal value with the saved EIP and hit enter. This should show where the exception takes place as seen below on the left-most windowlet: https://bitbucket.org/adeko1/release-debugging/src/master/windbgFinal.png

Done.

Return back home (https://bitbucket.org/adeko1/release-debugging/wiki/Home)